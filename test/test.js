require('strict-mode')(function () {
    const mongoose = require('mongoose');
    const Product = require('../models/product.model');

    //Require the dev-dependencies
    const chai = require('chai');
    const chaiHttp = require('chai-http');
    const server = require('../app');
    const should = chai.should();
    var expect = chai.expect;


    chai.use(chaiHttp);

    describe('/GET test', () => {
        it('it should GET test', (done) => {
        chai.request(server)
            .get('/products/test')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
            done();
            });
        });
    });

    describe('/GET product', () => {
        it('it should GET apple', (done) => {
            chai.request(server)
            .get('/products/:apple')
            .end((err, res) => {
                expect(res).to.have.status(200);
                res.body.should.be.a('object');
                expect(res.body).to.eql({});
                done();
            });
        });
    });

    describe('/GET product', () => {
        it('it should GET nonProduct', (done) => {
        chai.request(server)
            .get('/products/')
            .end((err, res) => {
                res.should.have.status(404);
                done();
            });
        });
    });

   describe('/POST product', () => {
        it('create a test product called mi5', (done) => {
           var product = {
                name:"mi5",
                price:100,
            }
            chai.request(server)
            .post('/products/create')
            .send(product)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });
    });

    describe('/PUT product', () => {
        it('it should update the product given the name', (done) => {
            chai.request(server)
            .put('/products/:mi5/update')
            .send({price:10000})
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });

        });
    });

    describe('/DELETE product', () => {
        it('it should DELETE a product given the name', (done) => {
            chai.request(server)
            .delete('/products/:mi5/delete')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });
    });

});



